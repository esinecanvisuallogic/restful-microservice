package org.esinecan.service.specification;

import dtos.CustomerDTO;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.EmailExistsException;
import org.esinecan.exception.IdExistsException;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
public interface CustomerService {
    CustomerDTO putNewCustomer(Long customerId, CustomerDTO customerDTO) throws IdExistsException, EmailExistsException;

    CustomerDTO getCustomer(Long customerId) throws CustomerNotFoundException;
}
