package org.esinecan.service.specification;

import dtos.ContractDTO;
import dtos.ContractType;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.exception.TypeNotFoundException;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
public interface ContractService {

    ContractDTO putNewContract(Long contractId, ContractDTO contractDTO) throws CustomerNotFoundException, IdExistsException, TypeNotFoundException;

    Double getContractSumOfCustomer(Long customerId) throws CustomerNotFoundException;

    Double getContractSumOfType(ContractType type) throws TypeNotFoundException;
}
