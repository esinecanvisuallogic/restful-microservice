package org.esinecan.service.implementation.hystrixfeign.client;

import dtos.ContractDTO;

import dtos.ContractType;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@FeignClient(value = "customer-contract-persistence")
public interface ContractClient {

    @RequestLine("POST /put-new-contract")
    ResponseEntity putNewContract(ContractDTO contractDTO);

    @RequestLine("POST /get-customer-revenue-sum")
    ResponseEntity getContractSumOfCustomer(Long customerId);

    @RequestLine("POST /get-type-revenue-sum")
    ResponseEntity getContractSumOfType(ContractType type);
}
