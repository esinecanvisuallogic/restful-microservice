package org.esinecan.service.implementation.hystrixfeign.command;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import dtos.CustomerDTO;
import org.esinecan.exception.EmailExistsException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.service.implementation.hystrixfeign.client.CustomerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class PutNewCustomerCommand {

    private final CustomerClient customerClient;

    private static final Logger logger = LoggerFactory.getLogger(PutNewCustomerCommand.class);

    @Autowired
    public PutNewCustomerCommand(CustomerClient customerClient){
        this.customerClient = customerClient;
    }

    @HystrixCommand(fallbackMethod = "putCustomerFallBack", commandKey = "PutNewCustomerCommand" )
    public CustomerDTO execute(CustomerDTO newCustomerDto) throws EmailExistsException, IdExistsException {
        ResponseEntity putCustomerResponse = customerClient.putNewCustomer(newCustomerDto);

        if (putCustomerResponse.getStatusCode() == HttpStatus.CONFLICT){
            throw new EmailExistsException(newCustomerDto.getEmail());
        }else if (putCustomerResponse.getStatusCode() == HttpStatus.FORBIDDEN){
            throw new IdExistsException(newCustomerDto.getId());
        }

        return (CustomerDTO) putCustomerResponse.getBody();
    }

    public CustomerDTO putCustomerFallBack(CustomerDTO newCustomerDto) throws Exception {
        logger.error("Customer creation failed unexpectedly: {}", newCustomerDto);
        throw (new Exception("A server error occured for customer: " + newCustomerDto));
    }
}
