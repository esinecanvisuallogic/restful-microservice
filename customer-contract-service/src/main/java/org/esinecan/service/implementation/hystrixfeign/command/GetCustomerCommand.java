package org.esinecan.service.implementation.hystrixfeign.command;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import dtos.CustomerDTO;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.EmailExistsException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.service.implementation.hystrixfeign.client.CustomerClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class GetCustomerCommand {

    private final CustomerClient customerClient;

    private static final Logger logger = LoggerFactory.getLogger(GetCustomerCommand.class);

    @Autowired
    public GetCustomerCommand(CustomerClient customerClient){
        this.customerClient = customerClient;
    }

    @HystrixCommand(fallbackMethod = "getCustomerFallBack", commandKey = "GetCustomerCommand" )
    public CustomerDTO execute(Long customerId) throws CustomerNotFoundException {
        ResponseEntity getCustomerResponse = customerClient.getCustomer(customerId);

        if (getCustomerResponse.getStatusCode() == HttpStatus.NOT_FOUND){
            throw new CustomerNotFoundException(customerId);
        }

        return (CustomerDTO) getCustomerResponse.getBody();
    }

    public CustomerDTO getCustomerFallBack(Long customerId) throws Exception {
        logger.error("Can't get Customer: {}", customerId);
        throw (new Exception("A server error occured for customer: " + customerId));
    }
}
