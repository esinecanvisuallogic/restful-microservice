package org.esinecan.service.implementation.hystrixfeign.command;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import dtos.ContractDTO;
import dtos.CustomerDTO;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.exception.TypeNotFoundException;
import org.esinecan.service.implementation.hystrixfeign.client.ContractClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class PutNewContractCommand {

    private final ContractClient contractClient;

    private static final Logger logger = LoggerFactory.getLogger(PutNewContractCommand.class);

    @Autowired
    public PutNewContractCommand(ContractClient contractClient){
        this.contractClient = contractClient;
    }

    @HystrixCommand(fallbackMethod = "putContractFallBack", commandKey = "PutNewContractCommand" )
    public ContractDTO execute(ContractDTO contractDTO)
            throws CustomerNotFoundException, IdExistsException, TypeNotFoundException {
        ResponseEntity contractResponse = contractClient.putNewContract(contractDTO);

        if(contractResponse.getStatusCode() == HttpStatus.FORBIDDEN){
            throw new IdExistsException(contractDTO.getId());
        }else if(contractResponse.getStatusCode() == HttpStatus.NOT_FOUND){
            throw new CustomerNotFoundException(contractDTO.getCustomerId());
        }else if(contractResponse.getStatusCode() == HttpStatus.FAILED_DEPENDENCY){
            throw new TypeNotFoundException(contractDTO.getType().getValue());
        }

        return (ContractDTO) contractResponse.getBody();
    }

    public ContractDTO putContractFallBack(ContractDTO contractDTO) throws Exception {
        logger.error("Contract creation failed unexpectedly: {}", contractDTO);
        throw (new Exception("A server error occured for contract: " + contractDTO));
    }
}
