package org.esinecan.service.implementation;

import dtos.CustomerDTO;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.EmailExistsException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.service.implementation.hystrixfeign.command.GetCustomerCommand;
import org.esinecan.service.implementation.hystrixfeign.command.PutNewCustomerCommand;
import org.esinecan.service.specification.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class CustomerServiceImpl implements CustomerService{

    @Autowired
    private PutNewCustomerCommand putNewCustomerCommand;

    @Autowired
    private GetCustomerCommand getCustomerCommand;

    @Override
    public CustomerDTO putNewCustomer(Long customerId, CustomerDTO customerDTO) throws IdExistsException, EmailExistsException {
        //PathVariable trumps over customerDTO
        if(customerId !=  null && customerId.intValue() != 0){
            customerDTO.setId(customerId);
        }
        return putNewCustomerCommand.execute(customerDTO);
    }

    @Override
    public CustomerDTO getCustomer(Long customerId) throws CustomerNotFoundException {
        return getCustomerCommand.execute(customerId);
    }
}
