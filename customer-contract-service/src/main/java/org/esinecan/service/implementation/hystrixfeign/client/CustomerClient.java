package org.esinecan.service.implementation.hystrixfeign.client;

import dtos.CustomerDTO;
import feign.RequestLine;
import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.http.ResponseEntity;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@FeignClient(value = "customer-contract-persistence")
public interface CustomerClient {
    @RequestLine("POST /put-new-contract")
    ResponseEntity putNewCustomer(CustomerDTO customerDTO);

    @RequestLine("POST /get-user")
    ResponseEntity getCustomer(Long customerId);
}
