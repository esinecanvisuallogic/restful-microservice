package org.esinecan.service.implementation;

import dtos.ContractDTO;
import dtos.ContractType;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.exception.TypeNotFoundException;
import org.esinecan.service.implementation.hystrixfeign.command.GetContractSumOfCustomerCommand;
import org.esinecan.service.implementation.hystrixfeign.command.GetContractSumOfTypeCommand;
import org.esinecan.service.implementation.hystrixfeign.command.PutNewContractCommand;
import org.esinecan.service.specification.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class ContractServiceImpl implements ContractService{

    @Autowired
    private GetContractSumOfCustomerCommand getContractSumOfCustomerCommand;

    @Autowired
    private GetContractSumOfTypeCommand getContractSumOfTypeCommand;

    @Autowired
    private PutNewContractCommand putNewContractCommand;

    @Override
    public ContractDTO putNewContract(Long contractId, ContractDTO contractDTO)
            throws CustomerNotFoundException, IdExistsException, TypeNotFoundException {
        //PathVariable trumps over contractDTO
        if(contractId !=  null && contractId.intValue() != 0){
            contractDTO.setId(contractId);
        }
        return putNewContractCommand.execute(contractDTO);
    }

    @Override
    public Double getContractSumOfCustomer(Long customerId) throws CustomerNotFoundException {
        return getContractSumOfCustomerCommand.execute(customerId);
    }

    @Override
    public Double getContractSumOfType(ContractType type) throws TypeNotFoundException {
        return getContractSumOfTypeCommand.execute(type);
    }
}
