package org.esinecan.service.implementation.hystrixfeign.command;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import dtos.ContractDTO;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.service.implementation.hystrixfeign.client.ContractClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class GetContractSumOfCustomerCommand {
    private final ContractClient contractClient;

    private static final Logger logger = LoggerFactory.getLogger(GetContractSumOfCustomerCommand.class);

    @Autowired
    public GetContractSumOfCustomerCommand(ContractClient contractClient){
        this.contractClient = contractClient;
    }

    @HystrixCommand(fallbackMethod = "getContractSumOfCustomerFallBack", commandKey = "GetContractSumOfCustomerCommand" )
    public Double execute(Long customerId) throws CustomerNotFoundException {
        ResponseEntity contractResponse = contractClient.getContractSumOfCustomer(customerId);

        if(contractResponse.getStatusCode() == HttpStatus.NOT_FOUND){
            throw new CustomerNotFoundException(customerId);
        }

        return (Double) contractResponse.getBody();
    }

    public Double getContractSumOfCustomerFallBack(Long customerId) throws Exception {
        logger.error("Can't get Customer: {}", customerId);
        throw (new Exception("A server error occured for contracts of customer: " + customerId));
    }
}
