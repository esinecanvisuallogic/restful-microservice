package org.esinecan.service.implementation.hystrixfeign.command;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import dtos.ContractType;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.TypeNotFoundException;
import org.esinecan.service.implementation.hystrixfeign.client.ContractClient;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class GetContractSumOfTypeCommand {

    private final ContractClient contractClient;

    private static final Logger logger = LoggerFactory.getLogger(GetContractSumOfTypeCommand.class);

    @Autowired
    public GetContractSumOfTypeCommand(ContractClient contractClient){
        this.contractClient = contractClient;
    }

    @HystrixCommand(fallbackMethod = "getContractSumOfTypeFallBack", commandKey = "GetContractSumOfTypeCommand" )
    public Double execute(ContractType type) throws TypeNotFoundException {
        ResponseEntity contractResponse = contractClient.getContractSumOfType(type);
        if(contractResponse.getStatusCode() == HttpStatus.FAILED_DEPENDENCY){
            throw new TypeNotFoundException(type.getValue());
        }
        return (Double) contractResponse.getBody();
    }

    public Double getContractSumOfTypeFallBack(ContractType type) throws Exception {
        logger.error("Can't get contracts of type: {}", type);
        throw (new Exception("A server error occured for contracts of type: " + type));
    }
}
