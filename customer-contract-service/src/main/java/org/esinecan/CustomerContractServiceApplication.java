package org.esinecan;

import org.esinecan.config.FeignClientConfiguration;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.hystrix.EnableHystrix;

@SpringBootApplication
@EnableEurekaClient
@EnableFeignClients(defaultConfiguration = FeignClientConfiguration.class)
@EnableHystrix
public class CustomerContractServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(CustomerContractServiceApplication.class, args);
	}
}
