package org.esinecan.web;

import dtos.ContractDTO;
import dtos.ContractType;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.EmailExistsException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.exception.TypeNotFoundException;
import org.esinecan.service.specification.ContractService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@RestController
@RequestMapping("/contract")
public class ContractController {

    @Autowired
    private ContractService contractService;

    @RequestMapping(value = "/{contract_id}", method = RequestMethod.PUT)
    public ContractDTO putContract(@PathVariable(value="contract_id") Long contractId,
                                   @RequestBody @Valid ContractDTO contractDTO)
            throws CustomerNotFoundException, IdExistsException, TypeNotFoundException {
        return contractService.putNewContract(contractId, contractDTO);
    }

    @RequestMapping(value = "/{customer_id}", method = RequestMethod.GET)
    public Double getContractSumOfCustomer(@PathVariable(value="customer_id") Long customerId)
            throws CustomerNotFoundException {
        return contractService.getContractSumOfCustomer(customerId);
    }

    @RequestMapping(value = "/{type}", method = RequestMethod.GET)
    public Double getContractSumOfType(@PathVariable(value="type")ContractType type) throws TypeNotFoundException{
        return contractService.getContractSumOfType(type);
    }
}
