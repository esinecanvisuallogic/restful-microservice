package org.esinecan.web;

import dtos.CustomerDTO;
import org.esinecan.exception.CustomerNotFoundException;
import org.esinecan.exception.EmailExistsException;
import org.esinecan.exception.IdExistsException;
import org.esinecan.service.specification.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@RestController
@RequestMapping("/customer")
public class CustomerController {

    @Autowired
    private CustomerService customerService;

    @RequestMapping(value = "/{customer_id}", method = RequestMethod.PUT)
    public CustomerDTO putCustomer(@PathVariable(value="customer_id") Long customerId,
                                      @RequestBody @Valid CustomerDTO customerDTO)
            throws EmailExistsException, IdExistsException {
        return customerService.putNewCustomer(customerId, customerDTO);
    }

    @RequestMapping(value = "/{customer_id}", method = RequestMethod.GET)
    public CustomerDTO getCustomer(@PathVariable(value="customer_id") Long customerId) throws CustomerNotFoundException {
        return customerService.getCustomer(customerId);
    }
}
