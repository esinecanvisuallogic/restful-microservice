package org.esinecan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@ResponseStatus(HttpStatus.NOT_FOUND)
public class CustomerNotFoundException extends Throwable{
    public CustomerNotFoundException(final Long id){
        super("Customer with Id:" + String.valueOf(id) + " can't be found");
    }
}
