package org.esinecan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.FORBIDDEN)
public class IdExistsException extends Throwable {

    public IdExistsException(final Long id) {
        super("Id " + String.valueOf(id) + " is already taken");
    }

}
