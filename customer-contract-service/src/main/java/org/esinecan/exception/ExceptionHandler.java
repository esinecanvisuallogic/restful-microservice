package org.esinecan.exception;

import exception.BaseExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

@RestControllerAdvice
public class ExceptionHandler extends BaseExceptionHandler {
    public ExceptionHandler(){
        super();
    }
}