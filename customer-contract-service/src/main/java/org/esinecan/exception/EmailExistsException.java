package org.esinecan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.CONFLICT)
public class EmailExistsException extends Throwable {

    public EmailExistsException(final String email){
        super("Email " + email + " is already taken");
    }

}
