package org.esinecan.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@SuppressWarnings("serial")
@ResponseStatus(HttpStatus.FAILED_DEPENDENCY)
public class TypeNotFoundException extends Throwable {

    public TypeNotFoundException(final String type) {
        super("Type " + type + " is not found");
    }

}
