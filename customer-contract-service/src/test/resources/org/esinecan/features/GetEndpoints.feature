Feature: Get Endpoints

Narrative:
We should be able to use three GET endpoints. /customerservice/customer/$customer_id should return customer with
  their contracts. /customerservice/contract/$customer_id should return total revenue of all contracts for that
  customer. /customerservice/contract/$type should return contracts of the specified type.

Scenario: Registering a user through idempotent PUT endpoint

Given A Customer cust with id 666, fullName Test Testington, email test666@myprovider.org is present
  And A Contract cont with id 666, customerId 666, startDate 12-01-2017, type rent, revenue 5 is present
When A GET call is made to /customerservice/customer/666
Then A ResponseEntity resp with status 200 should return
  And CustomerDTO dto with id 123, fullName Test Testington, email test@myprovider.org is present inside response
  And A ContractDTO list of length 1 with cont inside should return.
When A GET call is made to /customerservice/contract/666
Then A ResponseEntity object response2 with status 200 should return
  And A ContractDTO list list of length 1 should be present inside response2.
  And A ContractDTO with id 666, customerId 666, startDate 12-01-2017, type rent, revenue 5 should be inside list
When A GET call is made to /customerservice/contract/rent
Then A ResponseEntity responseRent with status 200 should be returned this time
  And A ContractDTO list listRent that has a length of 1 should be present inside responseRent.
  And A ContractDTO with id 666, customerId 666, startDate 12-01-2017, type rent, revenue 5 should be inside listRent
When A GET call is made to /customerservice/contract/667 this time
Then A ResponseEntity responseNotFound with status 404 NOT FOUND should return
  And responseNotFound has a String body of Customer 667 not found