Feature: Put a New Contract

Narrative:
Creating a contract with an Id for the first time should be successful,
  but the second time should fail. Nonexistent userId also should fail.

Scenario: Registering a contract

Given Firstly ContractDTO dto with id null, CustomerId 123, startDate 12-01-2017, type mortgage, revenue 5 is present
When A PUT call is made to /customerservice/contract/123 with the dto
Then A ResponseEntity response with status 200 OK should return
  And with a ContractDTO with id 123, customerId 123, startDate 12-01-2017, type mortgage, revenue 5
Given Later ContractDTO dto2 with id null, customerId 123, startDate 14-01-2017, type mortgage, revenue 7 is present
When A PUT call is made to /customerservice/contract/123 with the dto2 this time
Then A ResponseEntity response with status 409 should be returned
  And response has a String body of "Id 123 is already taken"
Given Finally ContractDTO dto3 with id null, customerId 666, startDate 14-01-2017, type mortgage, revenue 7 is present
When A PUT call is made to /customerservice/contract/1234
Then A ResponseEntity response with status 404 should return
  And response has a String body "Customer 1234 not found"