Feature: Put a New Customer

Narrative:
Creating a customer for the first time should be successful,
  but the second time should fail

Scenario: Registering a user through idempotent PUT endpoint

Given When CustomerDTO dto with id null, fullName Test Testington, email test@myprovider.org is present
When A PUT call is made to /customerservice/customer/123 with dto
Then A ResponseEntity named response with status 200 should return
  And CustomerDTO with id 123, fullName Test Testington, email test@myprovider.org is present inside response
Given We have a CustomerDTO dtoJr with id null, fullName Test Testington Jr., email testJR@myprovider.org
When A PUT call is made to /customerservice/customer/123 with the dtoJr this time
Then A ResponseEntity called response2 with status 409 should return
  And response2 should have String body of "Id 123 is already taken"
Given CustomerDTO with id null, fullName Test Testington Sr., email test@myprovider.org is created
When A PUT call is made to /customerservice/customer/1234 endpoint
Then A ResponseEntity response3 with status 409 should return
  And response3 has a String body of "email test@myprovider.org is already taken"