package org.esinecan.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.CustomerContractServiceApplication;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = CustomerContractServiceApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PutNewCustomerSteps {
    @Given("^When CustomerDTO dto with id null, fullName Test Testington, email test@myprovider\\.org is present$")
    public void when_CustomerDTO_dto_with_id_null_fullName_Test_Testington_email_test_myprovider_org_is_present() throws Throwable {
        
        //TODO
    }

    @When("^A PUT call is made to /customerservice/customer/(\\d+) with dto$")
    public void a_PUT_call_is_made_to_customerservice_customer_with_dto(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ResponseEntity named response with status (\\d+) should return$")
    public void a_ResponseEntity_named_response_with_status_should_return(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^CustomerDTO with id (\\d+), fullName Test Testington, email test@myprovider\\.org is present inside response$")
    public void customerdto_with_id_fullName_Test_Testington_email_test_myprovider_org_is_present_inside_response(int arg1) throws Throwable {
        
        //TODO
    }

    @Given("^We have a CustomerDTO dtoJr with id null, fullName Test Testington Jr\\., email testJR@myprovider\\.org$")
    public void we_have_a_CustomerDTO_dtoJr_with_id_null_fullName_Test_Testington_Jr_email_testJR_myprovider_org() throws Throwable {
        
        //TODO
    }

    @When("^A PUT call is made to /customerservice/customer/(\\d+) with the dtoJr this time$")
    public void a_PUT_call_is_made_to_customerservice_customer_with_the_dtoJr_this_time(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ResponseEntity called response(\\d+) with status (\\d+) should return$")
    public void a_ResponseEntity_called_response_with_status_should_return(int arg1, int arg2) throws Throwable {
        
        //TODO
    }

    @Then("^response(\\d+) should have String body of \"([^\"]*)\"$")
    public void response_should_have_String_body_of(int arg1, String arg2) throws Throwable {
        
        //TODO
    }

    @Given("^CustomerDTO with id null, fullName Test Testington Sr\\., email test@myprovider\\.org is created$")
    public void customerdto_with_id_null_fullName_Test_Testington_Sr_email_test_myprovider_org_is_created() throws Throwable {
        
        //TODO
    }

    @When("^A PUT call is made to /customerservice/customer/(\\d+) endpoint$")
    public void a_PUT_call_is_made_to_customerservice_customer_endpoint(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ResponseEntity response(\\d+) with status (\\d+) should return$")
    public void a_ResponseEntity_response_with_status_should_return(int arg1, int arg2) throws Throwable {
        
        //TODO
    }

    @Then("^response(\\d+) has a String body of \"([^\"]*)\"$")
    public void response_has_a_String_body_of(int arg1, String arg2) throws Throwable {
        
        //TODO
    }
}
