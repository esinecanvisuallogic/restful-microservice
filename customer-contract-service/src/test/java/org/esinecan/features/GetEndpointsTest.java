package org.esinecan.features;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;
import org.junit.runner.RunWith;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@RunWith(Cucumber.class)
@CucumberOptions(features = "src/test/resources/org/esinecan/features/GetEndpoints.feature")
public class GetEndpointsTest {
}
