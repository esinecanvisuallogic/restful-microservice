package org.esinecan.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.CustomerContractServiceApplication;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = CustomerContractServiceApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class PutNewContractSteps {

    @Given("^Firstly ContractDTO dto with id null, CustomerId (\\d+), startDate (\\d+)-(\\d+)-(\\d+), type mortgage, revenue (\\d+) is present$")
    public void firstly_ContractDTO_dto_with_id_null_CustomerId_startDate_type_mortgage_revenue_is_present(int arg1, int arg2, int arg3, int arg4, int arg5) throws Throwable {
        //TODO
    }

    @When("^A PUT call is made to /customerservice/contract/(\\d+) with the dto$")
    public void a_PUT_call_is_made_to_customerservice_contract_with_the_dto(int arg1) throws Throwable {

        //TODO;
    }

    @Then("^A ResponseEntity response with status (\\d+) OK should return$")
    public void a_ResponseEntity_response_with_status_OK_should_return(int arg1) throws Throwable {
        
        //TODO;
    }

    @Then("^with a ContractDTO with id (\\d+), customerId (\\d+), startDate (\\d+)-(\\d+)-(\\d+), type mortgage, revenue (\\d+)$")
    public void with_a_ContractDTO_with_id_customerId_startDate_type_mortgage_revenue(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        
        //TODO;
    }

    @Given("^Later ContractDTO dto(\\d+) with id null, customerId (\\d+), startDate (\\d+)-(\\d+)-(\\d+), type mortgage, revenue (\\d+) is present$")
    public void later_ContractDTO_dto_with_id_null_customerId_startDate_type_mortgage_revenue_is_present(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        
        //TODO;
    }

    @When("^A PUT call is made to /customerservice/contract/(\\d+) with the dto(\\d+) this time$")
    public void a_PUT_call_is_made_to_customerservice_contract_with_the_dto_this_time(int arg1, int arg2) throws Throwable {
        
        //TODO;
    }

    @Then("^A ResponseEntity response with status (\\d+) should be returned$")
    public void a_ResponseEntity_response_with_status_should_be_returned(int arg1) throws Throwable {
        
        //TODO;
    }

    @Then("^response has a String body of \"([^\"]*)\"$")
    public void response_has_a_String_body_of(String arg1) throws Throwable {
        
        //TODO;
    }

    @Given("^Finally ContractDTO dto(\\d+) with id null, customerId (\\d+), startDate (\\d+)-(\\d+)-(\\d+), type mortgage, revenue (\\d+) is present$")
    public void finally_ContractDTO_dto_with_id_null_customerId_startDate_type_mortgage_revenue_is_present(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        
        //TODO;
    }

    @When("^A PUT call is made to /customerservice/contract/(\\d+)$")
    public void a_PUT_call_is_made_to_customerservice_contract(int arg1) throws Throwable {
        
        //TODO;
    }

    @Then("^A ResponseEntity response with status (\\d+) should return$")
    public void a_ResponseEntity_response_with_status_should_return(int arg1) throws Throwable {
        
        //TODO;
    }

    @Then("^response has a String body \"([^\"]*)\"$")
    public void response_has_a_String_body(String arg1) throws Throwable {
        
        //TODO;
    }
}
