package org.esinecan.features;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;
import org.esinecan.CustomerContractServiceApplication;
import org.springframework.boot.test.context.SpringBootContextLoader;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ContextConfiguration;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@ContextConfiguration(
        loader = SpringBootContextLoader.class,
        classes = CustomerContractServiceApplication.class
)
@SpringBootTest(webEnvironment= SpringBootTest.WebEnvironment.RANDOM_PORT)
public class GetEndpointsSteps {

    @Given("^A Customer cust with id (\\d+), fullName Test Testington, email test(\\d+)@myprovider\\.org is present$")
    public void a_Customer_cust_with_id_fullName_Test_Testington_email_test_myprovider_org_is_present(int arg1, int arg2) throws Throwable {
        
        //TODO
    }

    @Given("^A Contract cont with id (\\d+), customerId (\\d+), startDate (\\d+)-(\\d+)-(\\d+), type rent, revenue (\\d+) is present$")
    public void a_Contract_cont_with_id_customerId_startDate_type_rent_revenue_is_present(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        
        //TODO
    }

    @When("^A GET call is made to /customerservice/customer/(\\d+)$")
    public void a_GET_call_is_made_to_customerservice_customer(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ResponseEntity resp with status (\\d+) should return$")
    public void a_ResponseEntity_resp_with_status_should_return(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^CustomerDTO dto with id (\\d+), fullName Test Testington, email test@myprovider\\.org is present inside response$")
    public void customerdto_dto_with_id_fullName_Test_Testington_email_test_myprovider_org_is_present_inside_response(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ContractDTO list of length (\\d+) with cont inside should return\\.$")
    public void a_ContractDTO_list_of_length_with_cont_inside_should_return(int arg1) throws Throwable {
        
        //TODO
    }

    @When("^A GET call is made to /customerservice/contract/(\\d+)$")
    public void a_GET_call_is_made_to_customerservice_contract(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ResponseEntity object response(\\d+) with status (\\d+) should return$")
    public void a_ResponseEntity_object_response_with_status_should_return(int arg1, int arg2) throws Throwable {
        
        //TODO
    }

    @Then("^A ContractDTO list list of length (\\d+) should be present inside response(\\d+)\\.$")
    public void a_ContractDTO_list_list_of_length_should_be_present_inside_response(int arg1, int arg2) throws Throwable {
        
        //TODO
    }

    @Then("^A ContractDTO with id (\\d+), customerId (\\d+), startDate (\\d+)-(\\d+)-(\\d+), type rent, revenue (\\d+) should be inside list$")
    public void a_ContractDTO_with_id_customerId_startDate_type_rent_revenue_should_be_inside_list(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        
        //TODO
    }

    @When("^A GET call is made to /customerservice/contract/rent$")
    public void a_GET_call_is_made_to_customerservice_contract_rent() throws Throwable {
        
        //TODO
    }

    @Then("^A ResponseEntity responseRent with status (\\d+) should be returned this time$")
    public void a_ResponseEntity_responseRent_with_status_should_be_returned_this_time(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ContractDTO list listRent that has a length of (\\d+) should be present inside responseRent\\.$")
    public void a_ContractDTO_list_listRent_that_has_a_length_of_should_be_present_inside_responseRent(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ContractDTO with id (\\d+), customerId (\\d+), startDate (\\d+)-(\\d+)-(\\d+), type rent, revenue (\\d+) should be inside listRent$")
    public void a_ContractDTO_with_id_customerId_startDate_type_rent_revenue_should_be_inside_listRent(int arg1, int arg2, int arg3, int arg4, int arg5, int arg6) throws Throwable {
        
        //TODO
    }

    @When("^A GET call is made to /customerservice/contract/(\\d+) this time$")
    public void a_GET_call_is_made_to_customerservice_contract_this_time(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^A ResponseEntity responseNotFound with status (\\d+) NOT FOUND should return$")
    public void a_ResponseEntity_responseNotFound_with_status_NOT_FOUND_should_return(int arg1) throws Throwable {
        
        //TODO
    }

    @Then("^responseNotFound has a String body of Customer (\\d+) not found$")
    public void responsenotfound_has_a_String_body_of_Customer_not_found(int arg1) throws Throwable {
        
        //TODO
    }
}
