import dtos.ContractDTO;
import dtos.ContractType;
import dtos.CustomerDTO;
import org.esinecan.persistence.CustomerContractPersistenceApplication;
import org.esinecan.persistence.dal.CustomerContractRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.Instant;
import java.util.Date;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes = {CustomerContractPersistenceApplication.class})
public class PersistenceServiceTest {

    @Autowired
    private CustomerContractRepository customerContractRepository;

    private CustomerDTO savedCust;

    @Test
    public void putNewCustomer(){
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setEmail("test@test.com");
        customerDTO.setFullName("test test");
        ResponseEntity responseEntity = customerContractRepository.saveCustomer(customerDTO);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
        savedCust = (CustomerDTO) responseEntity.getBody();
    }

    @Test
    public void putNewCustomer_error(){
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setEmail("test@test.com");
        customerDTO.setFullName("test test");
        ResponseEntity responseEntity = customerContractRepository.saveCustomer(customerDTO);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.CONFLICT);
        customerDTO.setId(savedCust.getId());
        responseEntity = customerContractRepository.saveCustomer(customerDTO);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.FORBIDDEN);
    }

    @Test
    public void putNewContract(){
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setCustomerId(savedCust.getId());
        contractDTO.setRevenue(1000D);
        contractDTO.setStartDate(Date.from(Instant.now()));
        contractDTO.setType(ContractType.MORTGAGE);
        ResponseEntity<ContractDTO> responseEntity = customerContractRepository.saveContract(contractDTO);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
        savedCust = (CustomerDTO) customerContractRepository.getCustomer(savedCust.getId()).getBody();
    }

    @Test
    public void putNewContract_error(){
        ContractDTO contractDTO = new ContractDTO();
        contractDTO.setCustomerId(765L);
        contractDTO.setRevenue(1000D);
        contractDTO.setStartDate(Date.from(Instant.now()));
        contractDTO.setType(ContractType.MORTGAGE);
        ResponseEntity<ContractDTO> responseEntity = customerContractRepository.saveContract(contractDTO);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.NOT_FOUND);
    }

    @Test
    public void getContractSumOfCustomer(){
        ResponseEntity<Double> responseEntity = customerContractRepository.getContractSumOfCustomer(savedCust.getId());
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
        assertEquals(responseEntity.getBody(), new Double(1000));
    }

    @Test
    public void getContractSumOfCustomer_error(){
        ResponseEntity<Double> responseEntity = customerContractRepository.getContractSumOfCustomer(123L);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.NOT_FOUND);
    }

    @Test
    public void getContractSumOfType(){
        ResponseEntity<Double> responseEntity = customerContractRepository.getContractSumOfType(ContractType.MORTGAGE);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.OK);
        assertEquals(responseEntity.getBody(), new Double(1000));
    }

    @Test
    public void getCustomer_error(){
        ResponseEntity<CustomerDTO> responseEntity = customerContractRepository.getCustomer(123L);
        assertTrue(responseEntity.getStatusCode() == HttpStatus.NOT_FOUND);
    }
}
