package org.esinecan.persistence;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.context.annotation.ComponentScan;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@SpringBootApplication(scanBasePackages = {"org.esinecan.persistence"})
@EnableEurekaClient
@ComponentScan({"org.esinecan.persistence"})
public class CustomerContractPersistenceApplication {
    public static void main(String[] args) {
        SpringApplication.run(CustomerContractPersistenceApplication.class, args);
    }
}
