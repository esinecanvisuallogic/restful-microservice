package org.esinecan.persistence.dal;

import dtos.ContractDTO;
import dtos.ContractType;
import dtos.CustomerDTO;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.atomic.AtomicLong;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Component
public class CustomerContractRepository {
    //Keeps customer info along with up to date contract info per customer
    private static final Map<Long, CustomerDTO> CUSTOMERS = new ConcurrentHashMap<>();

    //Similar to adding index on a SQL database, this map serves us so that we can quickly tell if a contract ID is available
    private static final Map<Long, ContractDTO> CONTRACTS = new ConcurrentHashMap<>();

    //Keeps all contracts based on type. Exists so that we can quickly answer revenue sum of a type.
    private static final Map<ContractType, List<ContractDTO>> CONTRACTS_BY_TYPE = new ConcurrentHashMap<>();

    public static final AtomicLong CUSTOMER_ID_INDEX = new AtomicLong(1L);

    public static final AtomicLong CONTRACT_ID_INDEX = new AtomicLong(1L);

    static {
        Arrays.stream(ContractType.values()).forEach(type -> {
            CONTRACTS_BY_TYPE.put(type, new ArrayList<>());
        });
    }

    /**
     * Saves user into our in memory db. containsKey is O(1), ideally getNextAvailableId is always O(1) as it's a running
     * index, put is O(1), hence, time complexity is O(1)
     * @param customerDTO
     * @return
     */
    public ResponseEntity<CustomerDTO> saveCustomer(CustomerDTO customerDTO){
        ResponseEntity<CustomerDTO> customerDTOResponseEntity;
        if(customerDTO.getId() != null && customerDTO.getId().intValue() != 0){
            if(!isCustomerIdAvailable(customerDTO.getId())){
                customerDTOResponseEntity = new ResponseEntity<>((CustomerDTO) null, HttpStatus.FORBIDDEN);
            }else{
                customerDTO.setContracts(new ArrayList<>());
                CUSTOMERS.put(customerDTO.getId(), customerDTO);
                customerDTOResponseEntity = new ResponseEntity<>(customerDTO, HttpStatus.OK);
            }
        }else{
            Long id = getNextAvailableCustomerId();
            customerDTO.setId(id);
            customerDTO.setContracts(new ArrayList<>());
            CUSTOMERS.put(customerDTO.getId(), customerDTO);
            customerDTOResponseEntity = new ResponseEntity<>(customerDTO, HttpStatus.OK);
        }
        return customerDTOResponseEntity;
    }

    /**
     * O(1) complexity as don't ever iterate.
     * @param customerId
     * @return
     */
    public ResponseEntity<CustomerDTO> getCustomer(Long customerId){
        if(CUSTOMERS.containsKey(customerId)){
            return new ResponseEntity<>(CUSTOMERS.get(customerId), HttpStatus.OK);
        }
        return new ResponseEntity<>((CustomerDTO) null, HttpStatus.NOT_FOUND);
    }

    /**
     * O(1) complexity as don't ever iterate.
     * @param contractDTO
     * @return
     */
    public ResponseEntity<ContractDTO> saveContract(ContractDTO contractDTO){
        ResponseEntity<ContractDTO> contractDTOResponseEntity;
        if(contractDTO.getId() != null && contractDTO.getId().intValue() != 0){
            if(!isContractIdAvailable(contractDTO.getId())){
                contractDTOResponseEntity = new ResponseEntity<>((ContractDTO) null, HttpStatus.FORBIDDEN);
                return contractDTOResponseEntity;
            }
        }else{
            contractDTO.setId(getNextAvailableContractId());
        }
        if(!CUSTOMERS.containsKey(contractDTO.getCustomerId())){
            contractDTOResponseEntity = new ResponseEntity<>((ContractDTO) null, HttpStatus.NOT_FOUND);
        }else{
            if(!CONTRACTS_BY_TYPE.containsKey(contractDTO.getType())){
                contractDTOResponseEntity = new ResponseEntity<>((ContractDTO) null, HttpStatus.FAILED_DEPENDENCY);
            }else{
                CUSTOMERS.get(contractDTO.getCustomerId()).getContracts().add(contractDTO);
                CONTRACTS.put(contractDTO.getId(), contractDTO);
                CONTRACTS_BY_TYPE.get(contractDTO.getType()).add(contractDTO);
                contractDTOResponseEntity = new ResponseEntity<>(contractDTO, HttpStatus.OK);
            }
        }
        return contractDTOResponseEntity;
    }

    /**
     * O(n) complexity as stream iterates all if user's contracts. If we kept a running sum, could have been O(1)
     * @param customerId
     * @return
     */
    public ResponseEntity<Double> getContractSumOfCustomer(Long customerId){
        if(CUSTOMERS.containsKey(customerId)){
            Double sum = CUSTOMERS.get(customerId).getContracts()
                    .stream().mapToDouble(ContractDTO::getRevenue).sum();
            return new ResponseEntity<>(sum, HttpStatus.OK);
        }
        return new ResponseEntity<>((Double) null, HttpStatus.NOT_FOUND);
    }

    /**
     * O(n) complexity as stream iterates all if contracts of a type. If we kept a running sum, could have been O(1)
     * @param type
     * @return
     */
    public ResponseEntity<Double> getContractSumOfType(ContractType type){
        if(CONTRACTS_BY_TYPE.containsKey(type)){
            Double sum = CONTRACTS_BY_TYPE.get(type)
                    .stream().mapToDouble(ContractDTO::getRevenue).sum();
            return new ResponseEntity<>(sum, HttpStatus.OK);
        }
        return new ResponseEntity<>((Double) null, HttpStatus.FAILED_DEPENDENCY);
    }

    private Long getNextAvailableCustomerId(){
        while (!isCustomerIdAvailable(CUSTOMER_ID_INDEX.get())){
            CUSTOMER_ID_INDEX.set(CUSTOMER_ID_INDEX.incrementAndGet());
        }
        return CUSTOMER_ID_INDEX.get();
    }

    private Long getNextAvailableContractId(){
        while (!isContractIdAvailable(CONTRACT_ID_INDEX.get())){
            CONTRACT_ID_INDEX.set(CONTRACT_ID_INDEX.incrementAndGet());
        }
        return CONTRACT_ID_INDEX.get();
    }

    private boolean isCustomerIdAvailable(Long id){
        return CUSTOMERS.containsKey(id);
    }

    private boolean isContractIdAvailable(Long id){
        return CONTRACTS.containsKey(id);
    }
}
