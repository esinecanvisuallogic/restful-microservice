package org.esinecan.persistence.service.implementation;

import dtos.ContractDTO;
import dtos.ContractType;
import dtos.CustomerDTO;
import org.esinecan.persistence.dal.CustomerContractRepository;
import org.esinecan.persistence.service.specification.PersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Service
public class PersistenceServiceImpl implements PersistenceService{

    @Autowired
    private CustomerContractRepository repository;

    @Override
    public ResponseEntity putNewContract(ContractDTO contractDTO) {
        return repository.saveContract(contractDTO);
    }

    @Override
    public ResponseEntity getContractSumOfCustomer(Long customerId) {
        return repository.getContractSumOfCustomer(customerId);
    }

    @Override
    public ResponseEntity getContractSumOfType(ContractType type) {
        return repository.getContractSumOfType(type);
    }

    @Override
    public ResponseEntity putNewCustomer(CustomerDTO customerDTO) {
        return repository.saveCustomer(customerDTO);
    }

    @Override
    public ResponseEntity getCustomer(Long customerId) {
        return repository.getCustomer(customerId);
    }
}
