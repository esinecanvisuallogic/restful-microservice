package org.esinecan.persistence.service.specification;

import dtos.ContractDTO;
import dtos.ContractType;
import dtos.CustomerDTO;
import org.springframework.http.ResponseEntity;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
public interface PersistenceService {
    ResponseEntity putNewContract(ContractDTO contractDTO);

    ResponseEntity getContractSumOfCustomer(Long customerId);

    ResponseEntity getContractSumOfType(ContractType type);

    ResponseEntity putNewCustomer(CustomerDTO customerDTO);

    ResponseEntity getCustomer(Long customerId);
}
