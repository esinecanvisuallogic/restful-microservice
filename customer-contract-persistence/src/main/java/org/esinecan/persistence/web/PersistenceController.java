package org.esinecan.persistence.web;

import dtos.ContractDTO;
import dtos.ContractType;
import dtos.CustomerDTO;
import org.esinecan.persistence.service.specification.PersistenceService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@RestController
public class PersistenceController {

    @Autowired
    private PersistenceService persistenceService;

    @RequestMapping(value = "/put-new-contract", method = RequestMethod.POST)
    public ResponseEntity putNewContract(ContractDTO contractDTO){
        return persistenceService.putNewContract(contractDTO);
    }

    @RequestMapping(value = "/get-customer-revenue-sum", method = RequestMethod.POST)
    public ResponseEntity getContractSumOfCustomer(Long customerId){
        return persistenceService.getContractSumOfCustomer(customerId);
    }

    @RequestMapping(value = "/get-type-revenue-sum", method = RequestMethod.POST)
    public ResponseEntity getContractSumOfType(ContractType type){
        return persistenceService.getContractSumOfType(type);
    }

    @RequestMapping(value = "/put-new-customer", method = RequestMethod.POST)
    public ResponseEntity putNewCustomer(@RequestBody CustomerDTO customerDTO){
        return persistenceService.putNewCustomer(customerDTO);
    }

    @RequestMapping(value = "/get-user", method = RequestMethod.POST)
    public ResponseEntity getCustomer(Long customerId){
        return persistenceService.getCustomer(customerId);
    }
}
