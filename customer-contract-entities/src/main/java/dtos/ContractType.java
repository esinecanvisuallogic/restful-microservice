package dtos;

import com.fasterxml.jackson.annotation.JsonValue;

import java.util.Arrays;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
public enum ContractType {
    MORTGAGE("mortgage"),
    RENT("rent"),
    OTHER("other");

    private String value;

    private ContractType(String value){
        this.value = value;
    }

    @JsonValue
    public String getValue(){
        return value;
    }

    public static ContractType getContractTypeByValue(String value){
        return Arrays.stream(ContractType.values())
                .filter(e -> e.getValue().equals(value))
                .findFirst()
                .orElseThrow(() -> new IllegalArgumentException(String.format("Unsupported type %s.", value)));
    }
}
