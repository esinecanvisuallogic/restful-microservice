package dtos;

import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;

import javax.validation.constraints.NotNull;
import java.util.Date;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Data
public class ContractDTO {

    private Long id;

    @NotNull
    private Long customerId;

    @NotNull
    @JsonFormat(shape= JsonFormat.Shape.STRING, pattern="yyyy-MM-dd")
    private Date startDate;

    @NotNull
    private ContractType type;

    @NotNull
    private Double revenue;
}
