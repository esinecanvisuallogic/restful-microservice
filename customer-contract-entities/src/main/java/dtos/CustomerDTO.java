package dtos;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;
import validation.annotation.ValidEmail;

import javax.validation.constraints.NotNull;
import java.util.List;

/**
 * Created by eren.sinecan on 30/04/2017.
 */
@Data
public class CustomerDTO {

    private Long id;

    private List<ContractDTO> contracts;

    @NotNull
    @JsonProperty("full name") //To conform to the specs
    private String fullName;

    @NotNull
    @ValidEmail
    private String email;
}
