# Contract - Customer Application #

Restful service with some endpoints to save contracts and customers

### How to run ###
Assuming you start at the root folder

* mvn clean package
* cd customer-contract-service-registry
* mvn spring-boot:run
* cd ..
* cd customer-contract-service-persistence
* mvn spring-boot:run
* cd ..
* cd customer-contract-service
* mvn spring-boot:run
* cd ..

Then you can send requests to http://localhost:8080/customerservice/.. as specified in the spec file